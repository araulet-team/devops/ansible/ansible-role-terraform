# Ansible Role Terraform [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Role for installing Terraform on Ubuntu.

## Requirements
This role requires Ansible 2.6 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: git@gitlab.com:araulet-team/devops/ansible/ansible-role-terraform.git
  name: araulet.terraform
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

```yaml
roles:
  - role: araulet.terraform
    become: true
    vars:
      terraform_install: true
      terraform_version: "0.11.10"
    tags: [ 'terraform' ]
```
## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet